<?php namespace Keios\MyWebsite\Components;

use Cms\Classes\ComponentBase;

/**
 * Class CookieWarning
 *
 * @package Keios\Mywebsite\Components
 */
class CookieWarning extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Cookie Warning',
            'description' => 'Displays EU Cookie Warning',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'injectAssets' => [
                'title'       => 'keios.mywebsite::lang.strings.inject_assets',
                'description' => 'keios.mywebsite::lang.strings.inject_assets_desc',
                'type'        => 'checkbox',
                'default'     => true,
            ],
        ];
    }

    /**
     * Cookie Warning onRun Method
     */
    public function onRun()
    {
        if ($this->property('injectAssets')) {
            $this->addJs('/plugins/keios/mywebsite/assets/js/cookie.js');
            $this->addCss('/plugins/keios/mywebsite/assets/css/cookie.css');
        }
    }

    public function onLoadAnalytics()
    {
        // empty, method to refresh analytics partials in custom js for GDPR
    }

}