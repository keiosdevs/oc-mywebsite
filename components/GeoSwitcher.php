<?php namespace Keios\Mywebsite\Components;

use Cms\Classes\ComponentBase;
use Raccoon\GeoLocation\Facades\Geo;
use RainLab\Translate\Models\Locale as LocaleModel;
use RainLab\Translate\Classes\Translator;

class GeoSwitcher extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'GeoSwitcher Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun(){
        if(\Session::has('detected_locale')){
          return;
        }
        $locales = LocaleModel::listEnabled();
        $translator = Translator::instance();
        $geoLocation = Geo::getLocation($this->getIp());
//dd($locales);
        if(array_key_exists(strtolower($geoLocation->countryCode), $locales)){
           \Session::put('detected_locale', $geoLocation->countryCode);
           return \Redirect::to('/'.strtolower($geoLocation->countryCode));
        }
    }

    public function getIp(){
      if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }

      return $ip;
    }
}
