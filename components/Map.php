<?php namespace Keios\MyWebsite\Components;

use Cms\Classes\ComponentBase;
use Keios\MyWebsite\Models\Settings;

/**
 * Class Map
 *
 * @package Keios\Mywebsite\Components
 */
class Map extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'MyMap',
            'description' => 'keios.mywebsite::lang.keys.mymap_desc',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * MyLocation onRun method
     */
    public function onRun()
    {
        $settings = Settings::instance();
        $data = [
            'location'            => $settings->get('location'),
            'google_key'          => $settings->get('google_key'),
            'map_width'           => $settings->get('map_width'),
            'map_height'          => $settings->get('map_height'),
            'map_frameborder'     => $settings->get('map_frameborder'),
            'map_style'           => $settings->get('map_style'),
            'map_container_class' => $settings->get('map_container_class'),
        ];

        foreach (array_filter($data) as $key => $value) {
            $this->page[$key] = $value;
        }
    }

}