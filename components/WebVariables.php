<?php namespace Keios\MyWebsite\Components;

use Cms\Classes\ComponentBase;
use Keios\MyWebsite\Models\Settings;
use Cache;
use System\Models\File;

/**
 * Class WebVariables
 *
 * @package Keios\MyWebsite\Components
 */
class WebVariables extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.mywebsite::lang.keys.myvariables',
            'description' => 'keios.mywebsite::lang.keys.myvariables_desc',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * WebVariables onRun method
     */
    public function onRun()
    {
        try {
            $source = Settings::instance();
        } catch (\Exception $e) {
            \Log::error('Error retrieving settings: ' . $e->getMessage());
        }

        if (!Cache::has('mywebsite_settings')) {
            $toCache = $this->cacheImagePaths($source);
            Cache::rememberForever(
                'mywebsite_settings',
                function () use ($toCache) {
                    return $toCache;
                }
            );
        }

        $cachedSettings = Cache::get('mywebsite_settings');
        $settings = $source->value;

        if (!\is_array($settings)) {
            return;
        }

        $this->setProfileFields($source);
        $this->setWebsiteFields($source);
        $this->setImages($cachedSettings);
        $this->setRawUrl();

    }

    /**
     * @param object $source
     *
     * @return array
     */
    public function cacheImagePaths($source)
    {
        /** @var File $websiteIcon */
        $websiteIcon = $this->unbindClosures($source->website_icon);
        $websiteLogo = $this->unbindClosures($source->website_logo);
        $ogImage = $this->unbindClosures($source->og_image);
        $twitterImage = $this->unbindClosures($source->twitter_image);
        $windowsImage = $this->unbindClosures($source->windows_image);
        $windowsHorizontalImage = $this->unbindClosures($source->windows_horizontal_image);

        //dd($websiteIcon->emitterEventCollection);

        $toCache = [
            'website_icon'             => $websiteIcon->path ?? '',
            'website_logo'             => $websiteLogo->path ?? '',
            'og_image'                 => $ogImage->path ?? '',
            'twitter_image'            => $twitterImage->path ?? '',
            'windows_image'            => $windowsImage->path ?? '',
            'windows_horizontal_image' => $windowsHorizontalImage->path ?? '',
        ];

        return $toCache;
    }

    private function unbindClosures(File $file = null)
    {
        if ($file) {
            unset($file->emitterEventCollection, $file->translatable, $file->purgeable);
            $file->unbindEvent();

            return $file;
        }

        return null;
    }

    /**
     * @param $cachedSettings
     */
    private function setImages($cachedSettings)
    {
        $this->page['website_icon'] = $cachedSettings['website_icon'];
        $this->page['website_logo'] = $cachedSettings['website_logo'];
        $this->page['default_og_image'] = $cachedSettings['og_image'];
        $this->page['default_twitter_image'] = $cachedSettings['twitter_image'];
        $this->page['windows_image'] = $cachedSettings['windows_image'];
        $this->page['windows_horizontal_image'] = $cachedSettings['windows_horizontal_image'];
    }

    /**
     * @param object $source
     * @param string $param
     *
     * @return mixed
     */
    private function getSetting($source, $param)
    {
        if (class_exists('RainLab\Translate\Plugin')) {
            $source->initTranslatableContext();

            return $source->getAttributeTranslated($param);
        }

        return $source->get($param);
    }

    /**
     * @return array
     */
    private function getMyProfileFields()
    {
        return [
            'facebook',
            'twitter',
            'phone',
            'phone2',
            'phone',
            'phone',
            'phone5',
            'linkedin',
            'email',
            'email2',
            'email3',
            'email4',
            'email5',
            'youtube',
            'instagram',
            'dribble',
            'reddit',
            'googleplus',
            'skype',
            'tumblr',
        ];

    }

    /**
     * @return array
     */
    private function getMyWebsiteFields()
    {

        return [
            'website_charset',
            'website_title',
            'website_description',
            'website_keywords',
            'website_robots',
            'website_revisit',
            'website_domain',
            'default_og_title',
            'default_og_description',
            'default_og_type',
            'default_twitter_title',
            'default_twitter_description',
            'twitter_card',
            'twitter_site',
            'twitter_player',
            'twitter_player_width',
            'twitter_player_height',
            'twitter_player_stream',
            'twitter_player_stream_content_type',
            'company_name',
            'company_street',
            'company_zip',
            'company_city',
            'company_country',
            'company_vat',
            'default_tile_title',
            'default_tile_background',
            'default_tile_rss_feed',
        ];
    }

    /**
     * @param $key
     * @param $profile
     *
     * @return mixed|string
     */
    private function getLinkForProfile($key, $profile)
    {
        $links = [
            'facebook'   => 'https://facebook.com/<PROFILE>',
            'twitter'    => 'https://twitter.com/<PROFILE>',
            'phone'      => 'phone:<PROFILE>',
            'phone2'     => 'phone:<PROFILE>',
            'phone3'     => 'phone:<PROFILE>',
            'phone4'     => 'phone:<PROFILE>',
            'phone5'     => 'phone:<PROFILE>',
            'linkedin'   => 'http://linkedin.com/in/<PROFILE>',
            'email'      => 'mailto:<PROFILE>',
            'email2'     => 'mailto:<PROFILE>',
            'email3'     => 'mailto:<PROFILE>',
            'email4'     => 'mailto:<PROFILE>',
            'email5'     => 'mailto:<PROFILE>',
            'youtube'    => 'https://youtube.com/user/<PROFILE>',
            'instagram'  => 'https://www.instagram.com/<PROFILE>',
            'dribble'    => 'http:/dribble.com/<PROFILE>',
            'reddit'     => 'http://reddit.com/user/<PROFILE>',
            'googleplus' => 'https://plus.google.com/<PROFILE>',
            'skype'      => 'skype:<PROFILE>',
            'tumblr'     => $profile,
        ];
        if (array_key_exists($key, $links)) {
            return str_replace('<PROFILE>', $profile, $links[$key]);
        }

        return '';
    }

    private function setProfileFields($source)
    {
        foreach ($this->getMyProfileFields() as $profile) {
            if ($profileSetting = $this->getSetting($source, $profile)) {
                $this->page['my_' . $profile] = $profileSetting;
                $this->page['link_' . $profile] = $this->getLinkForProfile($profile, $profileSetting);
            }
        }
    }

    private function setWebsiteFields($source)
    {
        foreach ($this->getMyWebsiteFields() as $websiteField) {
            if ($websiteSetting = $this->getSetting($source, $websiteField)) {
                $this->page[$websiteField] = $websiteSetting;
            }
        }
    }

    private function setRawUrl()
    {
        $url = $this->page->url;
        if (isset($this->controllers->vars['this'])) {
            foreach ($this->controller->vars['this']['param'] as $key => $value) {
                $url = str_replace(':', '', $value) === $key ? str_replace([$value, '?'], '', $url) : str_replace(
                    [':' . $key, '?'],
                    [$value, ''],
                    $url
                );
            }
        }

        $this->page['page_raw_url'] = $url;
    }

}
