<?php namespace Keios\Mywebsite\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\Simplemenu\Models\Menu;
use Lang;

/**
 * Website Manager Back-end Controller
 */
class WebsiteManager extends Controller
{

    const PORTFOLIO = 'Keios\Portfolio\Models\Item';
    const FAQ = 'Keios\Faq\Models\FaqItem';
    const MENU = 'Keios\Simplemenu\Models\Menu';
    const SLIDER = 'Keios\RjSliders\Models\Slider';

    /**
     * WebsiteManager constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Mywebsite', 'mywebsite', 'websitemanager');
    }

    /**
     * @return mixed
     * @throws \SystemException
     */
    public function index()
    {

    }

    public function renderModulesToolbar()
    {
        $portfolio = class_exists(self::PORTFOLIO);
        $faq = class_exists(self::FAQ);
        $simpleMenu = class_exists(self::MENU);
        $slider = class_exists(self::SLIDER);

        //dd($portfolio, $faq, $simpleMenu, $slider);

        $this->vars['portfolio'] = $portfolio;
        $this->vars['faq'] = $faq;
        $this->vars['simpleMenu'] = $simpleMenu;
        $this->vars['slider'] = $slider;

        return $this->makePartial('modules_toolbar');
    }
}